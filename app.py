# https://stackabuse.com/deploying-a-flask-application-to-heroku/
from flask import Flask, request, jsonify
import gpt_2_simple as gpt2
import urllib

app = Flask(__name__)
class Model(object):

    def __init__(self):
        self.sess = gpt2.start_tf_sess()
        with self.sess.graph.as_default():
          gpt2.load_gpt2(self.sess, model_dir='checkpoint', model_name="run1")

    def predict(self, prompt):
        with self.sess.graph.as_default():
           text = gpt2.generate(self.sess,
              length=250,
              temperature=0.7,
              prefix="<|startoftext|>{}".format(prompt),
              truncate='<|endoftext|>',
              include_prefix=False,
              return_as_list=True)[0]
        return text

model = Model()

@app.route('/', methods=['GET'])
def index():
    # Retrieve the name from url parameter
    prefix = request.args.get("prefix", "")

    response = {}
    global model

    prefix = urllib.parse.unquote(prefix)
    response["MESSAGE"] = "{}{}".format(prefix, model.predict(prefix))

    # Return the response in json format
    return jsonify(response)

if __name__ == '__main__':
    # Threaded option to enable multiple instances for multiple user access support
    app.run(threaded=True, port=5000)
